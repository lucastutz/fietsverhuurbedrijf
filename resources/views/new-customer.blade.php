<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Nieuwe Klant Register</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="../resources/css/style.css">
</head>

<body>
	<div id="page-wrap">
		<h1>Registreer nu bij ons</h1>
		<div id="contact-area">
			<form method="post" action="contactengine.php">
				<ul>
					<li class="form-label">
						<label for="fname">Voornaam:</label>
						<input type="text" name="fname" id="fname" />
					</li>
					<li class="form-label">
						<label for="lname">Achternaam:</label>
						<input type="text" name="lname" id="lname" />
					</li>
					<li class="form-label">
						<label for="City">Woonplaats:</label>
						<input type="text" name="City" id="City" />
					</li>
					<li class="form-label">
						<label for="street">Straat:</label>
						<input type="text" name="street" id="street" />
					</li>
					<li class="form-label">
						<label for="house-number">Huisnummer:</label>
						<input type="text" name="street" id="house-number" />
					</li>
					<li class="form-label">
						<label for="postcode">Postcode:</label>
						<input type="text" name="postcode" id="postcode" />
					</li>
					<li class="form-label">
						<label for="Email">Email:</label>
						<input type="email" name="Email" id="Email" />
					</li>
					<li class="form-label">
						<label for="phone-number">Telefoonnummer:</label>
						<input type="tel" name="phone-number" id="phone-number" />
					</li>
				</ul>
				<input type="submit" name="submit" value="Opslaan" class="submit-button" />
			</form>
			<div style="clear: both;"></div>
		</div>
	</div>
</body>

</html>