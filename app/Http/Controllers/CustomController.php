<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\View;

class CustomController extends Controller
{
    public function newCustomer(): View
    {
        return View('new-customer');
    }
}
